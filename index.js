const app = express();
const express = require("express");
const bcrypt = require("bcrypt");
const cors = require('cors');
const connection = require('./conf/db');
require("dotenv").config();

const port = 3000;

// On créé la route
app.post('/register', (req, res) => {

    // Décompose la variable
    const { email, password } = req.body;

    // si email ou password sont falsy (donc vide) renvoyer le message d'erreur, sinon hasher le mot de passe
    if (!email || !password) {
        res
            .status(400)
            .json({ error: "Please specify both email and password" });
    } else {
        // Hasher le mot de passe, on s'atttend au mot de passe original, saltRounds, ...
        bcrypt.hash(password, 10, function (bcryptError, hashedPassword) {
            if (bcryptError) {
                res
                    .status(500)
                    .json({ error: bcryptError });
            } else {
                // Enregistrer les infos dans la BDD
                connection.query(
                    "INSERT INTO user(email, password) VALUES (?, ?)",
                    [email, password], 
                    (mysqlError, result) => {
                        if (mysqlError) {
                            res.status(500).json({ error : mysqlError });
                        } else {
                            // On retourne les infos insérées, sans le mot de passe (sécurité)
                            res.status(201).json({
                                id: result.insertId,
                                email,
                            });
                        }
                    }
                )

            }
        })
    }
})

app.post('/login', (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
      res
        .status(400)
        .json({ errorMessage: 'Please specify both email and password' });
    } else {
      // Aller chercher dans la base de données les infos voulues
      connection.query(
        `SELECT * FROM user WHERE email=?`,
        [email],
        (mysqlError, result) => {
          if (mysqlError) {
            res.status(500).json({ error: mysqlError });
          } else if (result.length === 0) {
            res.status(401).json({ error: 'This account doesn\'t exist' });
          } else {
            const user = result[0];
            // Récupérer le mot de passe haché dans la base de données
            const hashedPassword = user.password;
            // On utiliser la méthode compare avec bcrypt 
            bcrypt.compare(password, hashedPassword, function(bcryptError, passwordMatch) {
              if (bcryptError) {
                res
                  .status(500)
                  .json({ error: bcryptError });
            // Si passwordMatch est vrai, càd si les deux mo de passe correspondent, alors :
              } else if (passwordMatch) {
                // Retourner le compte connecté en n'affichant que l'identifiant et l'email, 
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: 'Invalid password' });
              }
            });
          }
        }
      );
    }
  });
  

